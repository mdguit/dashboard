import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import * as moment from "moment";
import {MatDialogRef} from "@angular/material/dialog";
import {ParentChildService} from "../../../services/parent-child.service";
import {DialogModel} from "../../../models/dialogModel";
import {MatDatepickerInputEvent} from "@angular/material/datepicker";
import {EmployeeService} from "../../../services/employee.service";
import {Shift} from "../../../models/shift";

@Component({
  selector: 'app-dashboard-popup',
  templateUrl: './dashboard-popup.component.html',
  styleUrls: ['./dashboard-popup.component.scss']
})
export class DashboardPopupComponent implements OnInit {
  @Output()
  dateChange: EventEmitter<MatDatepickerInputEvent<any>> = new EventEmitter();

  constructor(public dialogRef: MatDialogRef<DashboardPopupComponent>, private parentChild: ParentChildService, private employeeService: EmployeeService ) { }

  displayedColumns: string[] = ['shift', 'clockIn', 'clockOut', 'totalWorkedHours'];
  data: DialogModel[] = [];

  ngOnInit() {
    this.parentChild.currentSubscribtion
      .subscribe(value => {
        this.data = value;
      });
  }

  sortShifts(event, shifts, id) {
    const dateShifts = [];
    const eventDate = moment(this.getValue(event)).format('YYYY-MM-DD');
    shifts.forEach(val => {
      if(moment(val.clockIn).format('YYYY-MM-DD') === eventDate) {
        dateShifts.push(val);
      }
    })
    const allShifts = (dateShifts.length === 0) ? shifts : dateShifts;
    return {id: id, allShifts: allShifts};
  }

  getValue(event: Event): string {
    return (event.target as HTMLInputElement).value;
  }

  getTimeDifference(timeFrom, timeTo) {
    return moment.duration(moment(timeTo).diff(moment(timeFrom))).asHours().toFixed(2);
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onSave(data) {
    data.forEach(val => {
      this.employeeService.putEmployee(val.employees)
        .subscribe(async res => console.log('success'));

      this.employeeService.putShift(val.shifts)
        .subscribe(async res => console.log('success'));
    })
    this.parentChild.changeSubscribtion(data);
    this.dialogRef.close();
  }

}
