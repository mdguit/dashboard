import {AfterViewInit, ChangeDetectorRef, Component, Inject, OnInit} from '@angular/core';
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../models/employee";
import {catchError, throwError} from "rxjs";
import {Shifts} from "../../models/shifts";
import * as moment from "moment";
import {DetailEmployee} from "../../models/detailEmployee";
import {MatDialog} from "@angular/material/dialog";
import {DialogModel} from "../../models/dialogModel";
import {DashboardPopupComponent} from "./dashboard-popup/dashboard-popup.component";
import {ParentChildService} from "../../services/parent-child.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: []
})
export class DashboardComponent implements OnInit {

  employees: Employee[] | [];
  shifts: Shifts[] | [];
  totalHours: number;
  totalPaidForRegularHours: number;
  totalPaidForOvertimeHours: number;
  employeesDetail: DetailEmployee[] = [];
  isLoading = true;
  displayedColumns: string[] = ['employeeName', 'employeeEmail', 'totalWorkedHours', 'totalPaidByRegularRate', 'totalPaidByOvertimeHours', 'checked'];
  bulkEditModel: DialogModel[] = [];
  isBulkEditDisabled = true;

  constructor(
    private employeeService: EmployeeService,
    public dialog: MatDialog,
    private parentChild: ParentChildService,
  ) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(DashboardPopupComponent, {
      width: '100%',
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  updateCheckedList(element, checked)
  {
    if (checked.checked) {
      this.bulkEditModel.push({
        employees: this.employees.find(x => x['id'] === element.employeeId),
        shifts: this.shifts.find(x => x['id'] === element.employeeId),
        customShifts: null
      });
    } else {
      this.bulkEditModel.forEach((val,i) => {
        if (val.employees.id === element.employeeId) {
          this.bulkEditModel.splice(i, 1);
        }
      })
    }
    this.isBulkEditDisabled = (this.bulkEditModel.length === 0);
  }

  getData () {
    const workingHoursOfAllEmployees: number[] = [];
    const totalRegularPaymentsForAllEmployees: number[] = [];
    const totalOvertimePaymentsForAllEmployees: number[] = [];
    this.employeesDetail = [];
    this.bulkEditModel = [];

    this.employeeService.getEmployeeArray()
      .subscribe(async data => {
         this.employees = data;

        this.employeeService.getShiftsArray()
          .subscribe(async data => {
            this.shifts = data;

            this.shifts?.forEach(shift => {
              const currentEmployee = this.employees.find(x => x['id'] === shift.id);
              const currentTotalHours = [];
              const currentPaidByRegularRate = [];
              const currentPaidByOvertimeRate = [];

              shift.allShifts?.forEach(date => {
                const clockOut = moment(date.clockOut);
                const clockIn = moment(date.clockIn);
                const duration = moment.duration(clockOut.diff(clockIn)).asHours();
                workingHoursOfAllEmployees.push(duration);
                currentTotalHours.push(duration);

                if (duration <= 8) {
                  totalRegularPaymentsForAllEmployees.push(currentEmployee.hourlyRate * duration);
                  currentPaidByRegularRate.push(currentEmployee.hourlyRate * duration);
                }
                if (duration > 8) {
                  totalRegularPaymentsForAllEmployees.push(currentEmployee.hourlyRate * 8);
                  totalOvertimePaymentsForAllEmployees.push(currentEmployee.overtimeHourlyRate * (duration - 8));
                  currentPaidByRegularRate.push(currentEmployee.hourlyRate * 8);
                  currentPaidByOvertimeRate.push(currentEmployee.overtimeHourlyRate * (duration - 8));
                }
              })

              this.employeesDetail.push({
                employeeId: currentEmployee.id,
                employeeName: currentEmployee.name,
                employeeEmail: currentEmployee.email,
                totalWorkedHours: +(currentTotalHours.reduce((a,b) => a + b, 0).toFixed(2)),
                totalPaidByRegularRate: +(currentPaidByRegularRate.reduce((a,b) => a + b, 0).toFixed(2)),
                totalPaidByOvertimeHours: +(currentPaidByOvertimeRate.reduce((a,b) => a + b, 0).toFixed(2))
              })

            })

            this.totalHours = +(workingHoursOfAllEmployees.reduce((a,b) => a + b, 0).toFixed(2));
            this.totalPaidForRegularHours = +(totalRegularPaymentsForAllEmployees.reduce((a,b) => a + b, 0).toFixed(2));
            this.totalPaidForOvertimeHours = +(totalOvertimePaymentsForAllEmployees.reduce((a,b) => a + b, 0).toFixed(2));
            this.employeesDetail = [...this.employeesDetail];
            this.parentChild.changeSubscribtion(this.bulkEditModel);
            this.isLoading = false;
          })
      })
  }

  ngOnInit() {

    this.getData();

    this.parentChild.currentSubscribtion
      .subscribe(val => {
        if (val.length !== 0) {
          this.getData();
        }
        this.isBulkEditDisabled = (this.bulkEditModel.length === 0);
      })
  }

}
