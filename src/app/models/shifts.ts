import {Shift} from "./shift";

export class Shifts {
  id: number;
  allShifts: [Shift];
}
