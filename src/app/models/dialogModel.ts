import {Employee} from "./employee";
import {Shifts} from "./shifts";

export class DialogModel {
  employees: Employee;
  shifts: Shifts;
  customShifts: {};
}
