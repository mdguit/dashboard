export class Employee {
  id: number | undefined;
  name: string;
  email: string;
  hourlyRate: number;
  overtimeHourlyRate: number;
}
