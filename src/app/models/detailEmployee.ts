export class DetailEmployee {
  employeeId: number;
  employeeName: string;
  employeeEmail: string;
  totalWorkedHours: number;
  totalPaidByRegularRate: number;
  totalPaidByOvertimeHours: number;
}
