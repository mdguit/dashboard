import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {DialogModel} from "../models/dialogModel";

@Injectable({
  providedIn: 'root'
})
export class ParentChildService {

  private subscribe = new BehaviorSubject<DialogModel[]>([]);
  currentSubscribtion = this.subscribe.asObservable();

  constructor() { }

  changeSubscribtion(message: DialogModel[]) {
    this.subscribe.next(message);
  }

}
