import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {catchError, Observable, throwError, timeout} from "rxjs";
import {Employee} from "../models/employee";
import {Shifts} from "../models/shifts";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  //This service provides API calls

  private employeesURL = '/api/employees';
  private shiftsURL = '/api/shifts';
  private simulateTimeOut = Math.random() * (2000 - 1800) + 1800;

  constructor(private http: HttpClient) { }

  //this method returns array of information concerning all employees

  getEmployeeArray(): Observable<Employee[]> {
    return this.http.get<Employee []>(this.employeesURL).pipe(
      timeout(this.simulateTimeOut),
      catchError(err => {
        console.error(err.message);
        return throwError(err);
      })
    );
  }
  //this method returns array of information concerning all shift of all employees

  getShiftsArray(): Observable<Shifts[]> {
    return this.http.get<Shifts []>(this.shiftsURL).pipe(
      timeout(this.simulateTimeOut),
      catchError(err => {
        console.error(err.message);
        return throwError(err);
      })
    );
  }

  //this method updates employee's information

  putEmployee(employee: Employee): Observable<any> {
    return this.http.put(this.employeesURL + '/' + employee.id, employee).pipe(
      timeout(this.simulateTimeOut),
      catchError(err => {
        console.error(err.message);
        return throwError(err);
      })
    )
  }

  //this method updates employee's shift's information

  putShift(shift: Shifts): Observable<any> {
    return this.http.put(this.shiftsURL + '/' + shift.id, shift).pipe(
      timeout(this.simulateTimeOut),
      catchError(err => {
        console.error(err.message);
        return throwError(err);
      })
    )
  }


}
